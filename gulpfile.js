'use strict'

var gulp = require('gulp'),
  concat = require('gulp-concat'),
  uglify = require('gulp-uglify'),
  rename = require('gulp-rename'),
  sass = require('gulp-sass'),
  cleanCSS = require('gulp-clean-css'),
  sourcemaps = require('gulp-sourcemaps'),
  rimraf = require('gulp-rimraf'),
  autoprefixer = require('gulp-autoprefixer'),
  imagemin = require('gulp-imagemin')


/*========================================
  JS
========================================*/

gulp.task('js', function() {
	return gulp.src('src/js/*.js')
		.pipe(sourcemaps.init())
		.pipe(concat('script.js'))
		.pipe(sourcemaps.write())
    .pipe(gulp.dest('build/js'))
        .pipe(rename('script.min.js'))
        .pipe(sourcemaps.init({ loadMaps: true }))
        .pipe(uglify())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('build/js'));
})


/*========================================
  SASS-CSS
========================================*/

gulp.task('css', function() {
	return gulp.src('src/scss/style.scss')
    .pipe(sourcemaps.init())
    .pipe(autoprefixer({
        browsers: ['last 2 versions'],
        cascade: false
    }))
    .pipe(sass())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('build/css'))
        .pipe(sourcemaps.init({ loadMaps: true }))
        .pipe(cleanCSS())
        .pipe(rename('style.min.css'))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('build/css'))
})


/*========================================
  TASKS
========================================*/
gulp.task('copy-html', function() {
    gulp.src('src/*.html')
    // Perform minif  ication tasks, etc here
    .pipe(gulp.dest('build'));
});

gulp.task('copy-images', function() {
    gulp.src('src/images/*')
        .pipe(imagemin())
        .pipe(gulp.dest('build/images'))
});

gulp.task('watch', function() {
    gulp.watch('src/scss/**/*.scss', ['css'])
    gulp.watch('src/js/*.js', ['js'])
    gulp.watch('src/*.html', ['copy-html'])
    gulp.watch('src/images/*', ['copy-images'])
})

gulp.task('clean', function() {
	return gulp.src(['build'], { read: false })
		.pipe(rimraf())
})

gulp.task('build', ['js', 'css', 'copy-html', 'copy-images'])

gulp.task('default', ['clean'], function() {
    gulp.start('build')
})